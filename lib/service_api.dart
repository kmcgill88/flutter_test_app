import 'dart:convert';
import 'dart:io';
import 'package:http/io_client.dart';

class ServiceAPI {
  var client;
  var url = 'http://sample.rout.graphql.query.com';

  initializeClient(){
    client = new IOClient(HttpClient());
  }

  getName() async {
    var response = await client.get(url);
    var decodedResponse = json.decode(response.body);
    return decodedResponse['name'];
  }
}
