import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test_app/service_api.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:http/io_client.dart';


MockClient getMockClient() {
  return MockClient((request) async {
    return Response('{"name": "test"}', 200);
  });
}

void main() {
  test('correct initialize', () async {
    var s = ServiceAPI();
    s.initializeClient();
    expect(true, s.client is IOClient );
  });

  test('simple mock test', () async {
    var s = ServiceAPI();
    s.client = getMockClient();
    var name = await s.getName();
    expect("test", name);
  });
}
